# frozen_string_literal: true

Gem::Specification.new do |gem|
  gem.version = File.read('VERSION').chomp
  gem.date = File.mtime('VERSION').strftime('%Y-%m-%d')

  gem.name = 'floe-models'
  gem.homepage = 'https://www.penguinparadigm.com/'
  gem.license = 'Apache-2.0'
  gem.summary = 'Digital object modeling for Penguins.'
  gem.description = 'Digital object modeling in PCDM, but if it were done by ' \
                    'Penguins.'

  gem.authors = ['thomas(ina) johnson']
  gem.email = 'tomjohnson@ucsb.edu'

  gem.platform = Gem::Platform::RUBY
  gem.files = %w[README.md VERSION] + Dir.glob('lib/**/*.rb')
  gem.require_paths = %w[lib]

  gem.required_ruby_version = '>= 2.7'

  gem.add_dependency 'valkyrie', '~> 2.1'

  gem.add_development_dependency 'pry'
  gem.add_development_dependency 'rspec', '~> 3.9'
  gem.add_development_dependency 'rubocop'
end
