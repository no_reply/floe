# frozen_string_literal: true

require 'floe'
require 'floe/models/specs/hydra_works'

RSpec.describe Floe::File do
  subject(:resource) { described_class.new }
  let(:adapter)      { Valkyrie::Persistence::Memory::MetadataAdapter.new }

  it_behaves_like 'a File'
end
