# frozen_string_literal: true

require 'valkyrie'
require 'floe/models/resource'
require 'floe/models/collection'
require 'floe/models/file'
require 'floe/models/file_set'
require 'floe/models/work'

module Floe
  ##
  # Valkyrie-based PCDM models for Penguins.
  module Models
  end
end
