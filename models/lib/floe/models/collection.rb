# frozen_string_literal: true

module Floe
  ##
  # The base Valkyrie model for Collections in PCDM.
  #
  # @see https://pcdm.org
  class Collection < Resource
    attribute :member_ids, Valkyrie::Types::Set.of(Valkyrie::Types::ID)

    ##
    # @return [Boolean]
    def collection?
      true
    end
  end
end
