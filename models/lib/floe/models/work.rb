# frozen_string_literal: true

module Floe
  ##
  # The base Valkyrie model for Works in the PCDM Works domain model.
  #
  # @see https://pcdm.org/2016/02/16/works
  class Work < Resource
    attribute :member_ids, Valkyrie::Types::Array.of(Valkyrie::Types::ID)
                                                 .meta(ordered: true)

    ##
    # @return [Boolean]
    def work?
      true
    end
  end
end
