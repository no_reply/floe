# frozen_string_literal: true

module Floe
  ##
  # The base Valkyrie model for FileSets in the PCDM Works domain model.
  #
  # @see https://pcdm.org/2016/02/16/works
  class FileSet < Resource
    ##
    # @return [Boolean]
    def file_set?
      true
    end
  end
end
