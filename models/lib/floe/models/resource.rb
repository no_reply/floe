# frozen_string_literal: true

module Floe
  ##
  # The base Valkyrie model for PCDM resources.
  #
  # @see https://pcdm.org/
  class Resource < Valkyrie::Resource
    ##
    # @return [Boolean]
    def collection?
      false
    end

    ##
    # @return [Boolean]
    def file?
      false
    end

    ##
    # @return [Boolean]
    def file_set?
      false
    end

    ##
    # @return [Boolean]
    def pcdm_object?
      false
    end

    ##
    # @return [Boolean]
    def work?
      false
    end
  end
end
