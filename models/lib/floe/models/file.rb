# frozen_string_literal: true

module Floe
  ##
  ##
  # The base Valkyrie model for Files (metadata) in PCDM.
  #
  # @see https://pcdm.org/
  class File < Floe::Resource
    ##
    # @return [Boolean]
    def file?
      true
    end
  end
end
