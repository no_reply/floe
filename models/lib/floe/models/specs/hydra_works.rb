# frozen_string_literal: true

require 'valkyrie/specs/shared_specs'

RSpec.shared_examples 'a Resource' do
  it_behaves_like 'a Valkyrie::Resource' do
    let(:resource_klass) { described_class }
  end

  it { is_expected.to respond_to :collection? }
  it { is_expected.to respond_to :file? }
  it { is_expected.to respond_to :file_set? }
  it { is_expected.to respond_to :pcdm_object? }
  it { is_expected.to respond_to :work? }
end

RSpec.shared_examples 'belongs to collections' do
  subject(:model)     { persister.save(resource: described_class.new) }
  let(:persister)     { adapter.persister }
  let(:query_service) { adapter.query_service }

  describe 'collection membership' do
    let(:collection) { persister.save(resource: Floe::Collection.new) }

    it 'is in no collections by default' do
      expect(query_service.find_parents(resource: model)).to be_empty
    end

    it 'can be added to a collection' do
      add_to_collection = lambda do |collection, member|
        collection.member_ids = [member.id]
        persister.save(resource: collection)
      end

      expect { add_to_collection.call(collection, model) }
        .to change { query_service.find_parents(resource: model).map(&:id) }
        .from(be_empty)
        .to contain_exactly(collection.id)
    end

    it 'can be removed from a collection' do
      collection.member_ids << model.id

      expect { collection.member_ids -= [model.id] }
        .to change { query_service.find_parents(resource: model) }
        .to be_empty
    end
  end
end

RSpec.shared_examples 'belongs to works' do
  subject(:model)     { described_class.new }
  let(:persister)     { adapter.persister }
  let(:query_service) { adapter.query_service }
  let(:parent_work)   { persister.save(resource: Floe::Work.new) }

  describe 'work membership' do
    it 'is in no works' do
      expect(query_service.find_parents(resource: model)).to be_empty
    end

    context 'with a saved model' do
      subject(:model) { persister.save(resource: described_class.new) }

      it 'is in no works by default' do
        expect(query_service.find_parents(resource: model)).to be_empty
      end

      it 'can be added to works' do
        add_member = lambda do |work, member|
          work.member_ids = [member.id]
          persister.save(resource: work)
        end

        expect { add_member.call(parent_work, model) }
          .to change { query_service.find_parents(resource: model).map(&:id) }
          .from(be_empty)
          .to contain_exactly(parent_work.id)
      end

      it 'can be added to a work multiple times' do
        add_member_twice = lambda do |work, member|
          work.member_ids << member.id
          work.member_ids << member.id
          persister.save(resource: work)
        end

        expect { add_member_twice.call(parent_work, model) }
          .to change { query_service.find_members(resource: parent_work) }
          .to contain_exactly(model, model)
      end
    end
  end
end

RSpec.shared_examples 'a Collection' do
  it_behaves_like 'a Resource'
  it_behaves_like 'belongs to collections'

  it { is_expected.to be_collection }
end

RSpec.shared_examples 'a File' do
  it_behaves_like 'a Resource'

  it { is_expected.to be_file }
end

RSpec.shared_examples 'a FileSet' do
  it_behaves_like 'a Resource'
  it_behaves_like 'belongs to works'

  it { is_expected.to be_file_set }
end

RSpec.shared_examples 'a Work' do
  it_behaves_like 'a Resource'
  it_behaves_like 'belongs to collections'
  it_behaves_like 'belongs to works'

  it { is_expected.to be_work }
end
