# frozen_string_literal: true

OkComputer.mount_at = 'healthz'

if ENV['MEMCACHED_HOST']
  OkComputer::Registry
    .register 'cache', OkComputer::CacheCheck.new(ENV.fetch('MEMCACHED_HOST'))
end
