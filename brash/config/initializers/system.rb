# frozen_string_literal: true

Dry::Rails.container do
  # cherry-pick features
  config.features = %i[application_contract]

  # enable auto-registration in the lib dir
  # auto_register!('lib')
end
