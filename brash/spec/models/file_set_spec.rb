# frozen_string_literal: true

require 'rails_helper'

RSpec.describe FileSet do
  subject(:resource) { described_class.new }
  let(:adapter)      { Valkyrie::Persistence::Memory::MetadataAdapter.new }

  it_behaves_like 'a FileSet'
end
